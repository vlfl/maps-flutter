import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class PolygonsPage extends StatefulWidget {

  @override
  _PolygonsPageState createState() => _PolygonsPageState();
}

class _PolygonsPageState extends State<PolygonsPage> {


  @override
  Widget build(BuildContext context) {
    
    YandexMapController mapController;

    void onMapCreated(YandexMapController controller) async {
      mapController = controller;

      await mapController.move(
        point: Point(latitude: 53.9, longitude: 27.58),
        zoom: 12
      );


      mapController.addPolygon(
        Polygon(
          strokeWidth: 3,
          strokeColor: Colors.blue,
          fillColor: Colors.green,
          coordinates: [
            Point(
              latitude: 53.9, 
              longitude: 27.55
            ),
            Point(
              latitude: 53.92, 
              longitude: 27.54
            ),
            Point(
              latitude: 53.9, 
              longitude: 27.57
            ),
            
            Point(
              latitude: 53.89, 
              longitude: 27.59
            ),
          
          ]
        )
      );

      mapController.addPolygon(
        Polygon(
          strokeWidth: 3,
          strokeColor: Colors.red,
          fillColor: Colors.transparent,
          coordinates: [
            Point(
              latitude: 53.93, 
              longitude: 27.58
            ),
            Point(
              latitude: 53.92, 
              longitude: 27.56
            ),
            Point(
              latitude: 53.9, 
              longitude: 27.6
            ),
            
            Point(
              latitude: 53.89, 
              longitude: 27.61
            ),
          
          ]
        )
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Полигоны'),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: YandexMap(
          onMapCreated: onMapCreated
        )
      )
    );
  }
}
