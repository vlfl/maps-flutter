import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class PinsPage extends StatefulWidget {

  @override
  _PinsPageState createState() => _PinsPageState();
}

class _PinsPageState extends State<PinsPage> {


  @override
  Widget build(BuildContext context) {
    
    YandexMapController mapController;

    void onMapCreated(YandexMapController controller) async {
      mapController = controller;

      await mapController.move(
        point: Point(latitude: 53.9, longitude: 27.58),
        zoom: 12
      );
    }

    void onAdd() async {
      final point = await mapController.getTargetPoint();
      mapController.addPlacemark(
        Placemark(
          point: point,
          iconName: 'assets/pin.png',
          opacity: 1,
          scale: 0.75
        )
      );
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Координаты'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('${point.latitude.toStringAsFixed(4)} ${point.longitude.toStringAsFixed(4)}'),
                ],
              ),
            )
          );
        }
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Метки'),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            YandexMap(
              onMapCreated: onMapCreated
            ),
            Center(
              child: Image(
                width: 30,
                height: 30,
                color: Colors.green,
                image: AssetImage('assets/pin.png',),
              )
            ),
          ]
        )
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: onAdd,
        child: Text('+'),
      ),
    );
  }
}
