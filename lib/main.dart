import 'package:flutter/material.dart';
import 'package:maps_yandex_flutter/pages/pin/widget.dart';
import 'package:maps_yandex_flutter/pages/polygons/widget.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    
    YandexMapController mapController;

    void onPins() {
      Navigator.push(context, MaterialPageRoute(builder: (ctx)=> PinsPage()));
    }

    void onPolygons() {
      Navigator.push(context, MaterialPageRoute(builder: (ctx)=> PolygonsPage()));
    }

    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.2, right: MediaQuery.of(context).size.width * 0.2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            OutlineButton(
              onPressed: onPins,
              child: Text('Метки'),
            ),
            OutlineButton(
              onPressed: onPolygons,
              child: Text('Полигоны'),
            )
          ],
        )
      )
    );
  }
}
