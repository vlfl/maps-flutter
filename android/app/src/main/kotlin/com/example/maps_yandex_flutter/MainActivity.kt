package com.example.maps_yandex_flutter

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant
import com.yandex.mapkit.MapKitFactory

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        MapKitFactory.setApiKey("b0d257fa-46f4-41d7-a869-e6dedba09e82");
        GeneratedPluginRegistrant.registerWith(flutterEngine);
    }
}
